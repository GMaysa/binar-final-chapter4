import React from "react";

import { GoogleLogin } from "react-google-login";

export default function ButtonPrm(props) {
  return (
    <button
      onClick={props.onClick ? () => props.onClick() : null}
      className="px-4 py-2 rounded-full text-white bg-red-700 ease-in-out duration-300"
    >
      {props.children}
    </button>
  );
}

export function TagButton(props) {
  return (
    <button
      onClick={props.onClick ? () => props.onClick() : null}
      className="px-6 py-1 rounded-full border-[1px] text-white bg-zinc-900 border-zinc-800"
    >
      {props.children}
    </button>
  );
}

export function OutlineButton(props) {
  return (
    <button
      onClick={props.onClick ? () => props.onClick() : null}
      className="px-4 py-2 rounded-full border-[1px] text-red-500 border-red-700 duration-300 ease-in-out hover:bg-red-950 hover:text-white"
    >
      {props.children}
    </button>
  );
}

export function GoogleLoginButton() {
  const responseGoogle = (response) => {
    console.log(response);
  };

  return (
    <GoogleLogin
      clientId="<client-id>"
      buttonText="Login with Google"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={"single_host_origin"}
    />
  );
}
