import { useState } from "react";

// chakra ui lib
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  FormControl,
  Input,
  InputGroup,
  InputRightElement,
  Stack,
  ModalFooter,
  FormHelperText,
  FormErrorMessage
} from "@chakra-ui/react"
import { AtSignIcon, ViewIcon, ViewOffIcon } from "@chakra-ui/icons"

// import button component
import ButtonPrm from "../Buttons"

// import react icon
import {AiOutlineUser} from 'react-icons/ai'



export default function LoginModal({ isOpen, onClose }) {
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  
  // toggle password
  const [showPw, setShowPw] = useState(false)
  const [showConfPw, setShowConfPw] = useState(false)
  const handleShowPw = () => setShowPw(!showPw)
  const handleShowConfPw = () => setShowConfPw(!showConfPw)

  const handleFirstName = (event) => setFirstName(event.target.value)
  const handleLastName = (event) => setLastName(event.target.value)
  const handleEmail = (event) => setEmail(event.target.value)
  const handlePassword = (event) => setPassword(event.target.value)
  const handleConfirmPassword = (event) => setConfirmPassword(event.target.value)
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(`Login with email ${email} and password ${password}`)
    onClose();
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent bgColor="#1e1e1e" color="#fff">
        <ModalHeader display='flex' justifyContent='space-between' alignItems='center' marginBottom='8' borderBottom='1px solid #333'>
          Register
          <ModalCloseButton position='static'/>
        </ModalHeader>
        <ModalBody>
          <form onSubmit={handleSubmit}>
            <Stack spacing={3} gap={2}>

              {/* FIRSTNAME INPUT */}
              <FormControl isRequired>
                <InputGroup>
                  <Input
                    type="text"
                    placeholder="First Name"
                    value={firstName}
                    onChange={handleFirstName}
                    borderRadius="full"
                    />
                  <InputRightElement
                    pointerEvents="none"
                    children={<AiOutlineUser color="gray.300" />}
                    />
                </InputGroup>
              </FormControl>

              {/* LASTNAME INPUT */}
              <FormControl isRequired>
                <InputGroup>
                  <Input
                    type="text"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={handleLastName}
                    borderRadius="full"
                  />
                  <InputRightElement
                    pointerEvents="none"
                    children={<AiOutlineUser color="gray.300" />}
                  />
                </InputGroup>
              </FormControl>

              {/* EMAIL INPUT */}
              <FormControl isRequired>
                <InputGroup>
                  <Input
                    type="email"
                    placeholder="Email Address"
                    value={email}
                    onChange={handleEmail}
                    borderRadius="full"
                  />
                  <InputRightElement
                    pointerEvents="none"
                    children={<AtSignIcon color="gray.300" />}
                  />
                </InputGroup>
              </FormControl>
              
              {/* PASSWORD INPUT */}
              <FormControl isRequired isInvalid={password!= '' && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password)}>
                <InputGroup>
                  <Input
                    type={showPw ? 'text' : 'password'}
                    placeholder="Password"
                    value={password}
                    onChange={handlePassword}
                    borderRadius="full"
                  />
                  <InputRightElement color='gray.300' onClick={handleShowPw} cursor='pointer'>
                    {showPw ? <ViewOffIcon /> : <ViewIcon />}
                  </InputRightElement>
                </InputGroup>
                { !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password) && <FormErrorMessage>Your password must contain at least 8 characters, 1 number, 1 uppercase letter, 1 lowercase letter.</FormErrorMessage>}
              </FormControl>
              
              {/* CONFIRM PASSWORD INPUT */}
              <FormControl isRequired isInvalid={confirmPassword != '' && confirmPassword != password}>
                <InputGroup>
                  <Input
                    type={showConfPw ? 'text' : 'password'}
                    placeholder="Password Confirmation"
                    value={confirmPassword}
                    onChange={handleConfirmPassword}
                    borderRadius="full"
                  />
                  <InputRightElement color='gray.300' onClick={handleShowConfPw} cursor='pointer'>
                    {showConfPw ? <ViewOffIcon /> : <ViewIcon />}
                  </InputRightElement>
                </InputGroup>
                {(confirmPassword != '' && confirmPassword != password) && <FormErrorMessage>Your password is not similar to the previous one</FormErrorMessage> }
              </FormControl>

              <ModalFooter px='0' justifyContent='start'>
                <ButtonPrm>Register Now</ButtonPrm>
              </ModalFooter>
            </Stack>
          </form>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
