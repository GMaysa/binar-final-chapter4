import { useState } from "react";

// chakra ui lib
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  FormControl,
  Input,
  InputGroup,
  InputRightElement,
  Stack,
  ModalFooter,
} from "@chakra-ui/react";
import { AtSignIcon, ViewIcon, ViewOffIcon } from "@chakra-ui/icons";

import ButtonPrm, { GoogleLoginButton } from "../Buttons";

export default function LoginModal({ isOpen, onClose }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  
  // toggle password
  const [showPw, setShowPw] = useState(false)
  const handleShowPw = () => setShowPw(!showPw)

  const handleEmailChange = (event) => setEmail(event.target.value);
  const handlePasswordChange = (event) => setPassword(event.target.value);
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(`Login with email ${email} and password ${password}`);
    onClose();
};

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent bgColor="#1e1e1e" color="#fff">
        <ModalHeader display='flex' justifyContent='space-between' alignItems='center' marginBottom='8' borderBottom='1px solid #333'>
          Login
          <ModalCloseButton position='static'/>
        </ModalHeader>
        <ModalBody>
          <form onSubmit={handleSubmit}>
            <Stack spacing={3} gap={2}>

              <FormControl isRequired>
                <InputGroup>
                  <Input
                    type="email"
                    placeholder="Email Address"
                    value={email}
                    onChange={handleEmailChange}
                    borderRadius="full"
                  />
                  <InputRightElement
                    pointerEvents="none"
                    children={<AtSignIcon color="gray.300" />}
                  />
                </InputGroup>
              </FormControl>
              
              <FormControl isRequired>
                <InputGroup>
                  <Input
                    type={showPw ? 'text' : 'password'}
                    placeholder="Password"
                    value={password}
                    onChange={handlePasswordChange}
                    borderRadius="full"
                  />
                  <InputRightElement color='gray.300' onClick={handleShowPw} cursor='pointer'>
                    {showPw ? <ViewOffIcon /> : <ViewIcon />}
                  </InputRightElement>
                </InputGroup>
              </FormControl>

              <ModalFooter px='0' justifyContent='start'>
                <ButtonPrm>Login</ButtonPrm>
                <GoogleLoginButton />
              </ModalFooter>
            </Stack>
          </form>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
