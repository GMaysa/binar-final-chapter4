import React, { useEffect, useState } from "react";
import { UilSearch } from "@iconscout/react-unicons";
import ButtonPrm, { OutlineButton } from "./Buttons";

function Navbar({ onOpenLoginModal, onOpenRegisModal }) {
  const [navbarBackground, setNavbarBackground] = useState("bg-transparent");

  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 0) {
        setNavbarBackground("bg-black/75");
      } else {
        setNavbarBackground("bg-transparent");
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className={`fixed z-50 w-screen duration-300 ease-in-out backdrop-blur-sm ${navbarBackground} `}>
      <div className="container h-20 flex justify-between items-center text-white">
        <div className="font-semibold flex items-end text-2xl leading-none">
          T<span className="text-lg leading-none">MDB</span>
          <span className="text-red-700 text-lg leading-none">.API</span>
        </div>

        <div className="flex gap-4">
          <form className="border-white/20 border-[0.5px] rounded-full py-2 px-4  text-white/40 flex items-center min-w-fit w-3/12">
            <UilSearch size="16" />
            <input
              type="text"
              name="searchInput"
              placeholder="Search movies here ..."
              className="bg-transparent w-60 outline-none pl-2.5 text-sm placeholder:text-white/40"
            />
          </form>
          <OutlineButton onClick={onOpenRegisModal}>Register</OutlineButton>
          <ButtonPrm onClick={onOpenLoginModal}>Login</ButtonPrm>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
