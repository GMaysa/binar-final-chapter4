import React from "react";

function Cards(props) {
  const imgUrlOri = "https://image.tmdb.org/t/p/original";

  return (
    <div onClick={props.onClick ? () => props.onClick() : null} className="">
      <img src={imgUrlOri + props.poster_path} alt={props.title} />
    </div>
  );
}

export default Cards;
