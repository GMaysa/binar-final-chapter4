import React, { useEffect, useState } from "react";

// import button
import ButtonPrm, { TagButton } from "../components/Buttons";

// import slick
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

// import swiper
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

// get data popular
import { GetGenres, GetPopular } from "../api/api";

function Home() {
  const [genreId, setGenreId] = useState(0)
  // define active index slide
  const [slideIndex, setSlideIndex] = useState(0)

  const [data, setData] = useState({
    popular: [],
    genres: [],
  });

  // get popular data
  useEffect(() => {
    GetPopular().then((data) =>
      setData((prev) => ({ ...prev, popular: data.results }))
    );
    GetGenres().then(
      (data) => setData((prev) => ({ ...prev, genres: data.genres }))
      // console.log(data)
    );
  }, []);

  // image base url
  const imgUrlOri = "https://image.tmdb.org/t/p/original";
  const imgUrl500 = "https://image.tmdb.org/t/p/w500";

  // setting for react slick
  const settings = {
    centerMode: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySPeed: 2000,
    centerPadding: "60px",
    beforeChange: (curr, next) => setSlideIndex(next),
  };

  return (
    <div className="w-screen flex">
      <div className="page-container">
        {/* SECTION SLIDER */}

        {/* SLIDE */}
        <div className="flex w-full h-96 bg-black rounded-lg">
          <div className="w-1/3 h-full p-10 flex flex-col justify-between text-white">
            <div>
              <h2 className="text-white text-4xl tracking-wide mb-4">
                {data.popular[slideIndex]?.original_title}
              </h2>
              <div className="text-xs text-neutral-700">
                Release date:{" "}
                <p className="text-neutral-500">
                  {data.popular[slideIndex]?.release_date}
                </p>
              </div>
            </div>
            <div>
              <ButtonPrm>Watch Trial</ButtonPrm>
            </div>
          </div>

          <div
            className="w-2/3 h-full bg-cover bg-no-repeat bg-center rounded-r-lg"
            style={{
              backgroundImage: data.popular[slideIndex]?.backdrop_path
                ? `url(${imgUrlOri}${data.popular[slideIndex]?.backdrop_path})`
                : "",
            }}
          >
            <div className="w-full h-full bg-gradient-to-r from-black via-transparent to-transparent flex items-end justify-end">
              <div className="mySwiper | h-24 w-5/12 p-4 bg-black/20 backdrop-blur-sm rounded-ss-2xl rounded-br-lg m-0 text-white flex flex-col justify-center items-center">
                <Slider {...settings} className=" w-10/12">
                  {data.popular.slice(0, 8).map((item, id) => (
                    <img
                      className="px-2"
                      src={imgUrlOri + item.backdrop_path}
                      key={id}
                    />
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </div>

        {/* SECTION GENRES */}
        <section className="w-full mt-14">
          {/* GENRES */}
          <Swiper
            className="slideScrollbar h-10 w-full whitespace-nowrap overflow-x-scroll"
            slidesPerGroup={1}
            slidesPerView="auto"
            spaceBetween={10}
          >
            <SwiperSlide className="w-fit">
              <TagButton>All</TagButton>
            </SwiperSlide>
            {data.genres != null &&
              data.genres.map((item, id) => (
                <SwiperSlide className="w-fit" key={id}>
                  <TagButton>{item.name}</TagButton>
                </SwiperSlide>
                // console.log(data.genres)
              ))}
          </Swiper>

          <div className="wrap-movies">

          </div>
        </section>
      </div>
    </div>
  );
}

export default Home;
