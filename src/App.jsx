import './App.css'

import { Outlet, RouterProvider, createBrowserRouter } from "react-router-dom";

import { ChakraProvider, useDisclosure } from '@chakra-ui/react';

import Navbar from "./components/Navbar";
import Detail from "./pages/Detail";
import Home from "./pages/Home";
import { useState } from 'react';
import LoginModal from './components/Modals/LoginModal';
import RegisterModal from './components/Modals/RegisterModal';

export default function App() {

  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [isRegisterModalOpen, setIsRegisterModalOpen] = useState(false);

  const handleOpenLoginModal = () => setIsLoginModalOpen(true);
  const handleCloseLoginModal = () => setIsLoginModalOpen(false);
  const handleOpenRegisterModal = () => setIsRegisterModalOpen(true);
  const handleCloseRegisterModal = () => setIsRegisterModalOpen(false);

  const Layout = () => {
    return(
      <>
        <Navbar  onOpenLoginModal={handleOpenLoginModal} onOpenRegisModal={handleOpenRegisterModal} />
        <Outlet />
        <LoginModal isOpen={isLoginModalOpen} onClose={handleCloseLoginModal} />
        <RegisterModal isOpen={isRegisterModalOpen} onClose={handleCloseRegisterModal} />
      </>
    )
  }

  const route = createBrowserRouter([
    {
      path: '/',
      element: <Layout/>,
      children:[
        {
          path: '',
          element: <Home/>
        },
        {
          path: 'detail/:id',
          element: <Detail/>
        }
      ]
    }
  ])

  return (
    <ChakraProvider>
      <div className="w-screen h-screen bg-[#111]">
        <RouterProvider router={route} />
      </div>
    </ChakraProvider>
  )
}