import axios from "axios";

export function GetPopular() {
  return axios
    .get('https://api.themoviedb.org/3/movie/popular?api_key=bbfee8b0163d81b8e6acfa28b3bec8d5&language=en-US&page=1')
    .then((res) => res.data);
}

export function GetGenres() {
  return axios
    .get("https://api.themoviedb.org/3/genre/movie/list?api_key=bbfee8b0163d81b8e6acfa28b3bec8d5&language=en-US")
    .then((res) => res.data);
}
